#include "setTDRStyle.h"
#include "TTree.h"
#include "TFile.h"
#include "TH1.h"
#include "TH1D.h"
#include <vector>
void setTDRStyle();

TH1D *create1Dhisto(TString sample,TTree *tree,TString intLumi,TString cuts,TString branch,int bins,float xmin,float xmax,
                    bool useLog,int color, int style,TString name,bool norm, bool data);



void readNanoAODExample(TString treename_) {
  // example plot the impact on the leading jet of jet systematics

  setTDRStyle();
  gROOT->SetBatch(false);
  gStyle->SetOptStat(0);
  gStyle->SetOptFit(1);
  gStyle->SetPalette(1);
  TH1::SetDefaultSumw2(kTRUE);


  // get file and tree
  TFile *f_ = TFile::Open(treename_ , "READONLY");
  TTree *t_ = (TTree*)f_->Get("Events");


  // nanoAOD branch  
  std::vector<TString> branches_; branches_.clear();
  branches_.push_back("Jet_pt_nom");
  branches_.push_back("Jet_pt_jesTotalUp");
  branches_.push_back("Jet_pt_jesTotalDown");
  branches_.push_back("Jet_pt_jerUp");
  branches_.push_back("Jet_pt_jerDown");


  // some cosmetics;
  // darker color up, lighter color down
  std::vector<int> colors_; colors_.clear();
  std::vector<int> styles_; styles_.clear();
  colors_.push_back(1);       styles_.push_back(1);
  colors_.push_back(kBlue);   styles_.push_back(2);
  colors_.push_back(kBlue-7); styles_.push_back(2);
  colors_.push_back(kRed);    styles_.push_back(4);
  colors_.push_back(kRed-9);  styles_.push_back(4);


  // selection; need at least a jet
  TString cut = "nJet>0";

  // luminosity - currently placeholder
  TString lumi = "1.";
  
  std::vector<TH1D*> h1s_; h1s_.clear();
  for (unsigned int i0=0; i0<branches_.size(); ++i0) {
    TH1D *h1tmp_ = (TH1D*)create1Dhisto("example",t_,lumi,cut,branches_[i0],20,0.,100,false,colors_[i0],styles_[i0],
					branches_[i0],true,false);
    h1s_.push_back(h1tmp_);
  }


  // plot them
  TCanvas *c_ = new TCanvas("c_","c_",500,500);
  for (unsigned int i0=0; i0<h1s_.size(); ++i0) {
    if (i0==0) { 
      h1s_[i0]->GetXaxis()->SetTitle("p_{T}(jet) [GeV]");
      h1s_[i0]->GetYaxis()->SetTitle("a. u.");
      h1s_[i0]->Draw("HIST E0");
    }
    else { h1s_[i0]->Draw("HIST E0 sames"); } 
  }


}


TH1D *create1Dhisto(TString sample,TTree *tree,TString intLumi,TString cuts,TString branch,int bins,float xmin,float xmax,
                    bool useLog,int color, int style,TString name,bool norm, bool data) {
  TH1::SetDefaultSumw2(kTRUE);

  TH1D *hTemp = new TH1D(name,name,bins,xmin,xmax);
  tree->Project(name,branch,cuts);

  hTemp->SetFillColor(0);
  hTemp->SetLineWidth(3);
  hTemp->SetMarkerSize(0);
  hTemp->SetLineColor(color);
  hTemp->SetFillColor(0);
  hTemp->SetLineStyle(style);

  // ad overflow bin
  double error =0.; double integral = hTemp->IntegralAndError(bins,bins+1,error);
  hTemp->SetBinContent(bins,integral);
  hTemp->SetBinError(bins,error);

  if (norm) { hTemp->Scale(1./(hTemp->Integral())); }

  return hTemp;
} //~ end of create1Dhisto
